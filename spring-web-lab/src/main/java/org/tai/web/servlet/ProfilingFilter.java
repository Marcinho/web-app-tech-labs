package org.tai.web.servlet;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.common.base.Stopwatch;

/**
 * @author Przemyslaw Dadel
 */
public class ProfilingFilter implements Filter {


    private static final Logger LOGGER = LoggerFactory.getLogger(ProfilingFilter.class);
    
    private PerSessionRequestCounter counterSession;
	private PerSessionRequestCounter counterGlobal;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    	ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(filterConfig.getServletContext());
    	counterSession = (PerSessionRequestCounter) context.getBean("counterSession");
    	counterGlobal = (PerSessionRequestCounter) context.getBean("counterGlobal");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final Stopwatch stopwatch = Stopwatch.createStarted();
        try {
            filterChain.doFilter(servletRequest, servletResponse);
        } finally {
            LOGGER.info("Processing request to {} took {} ms", ((HttpServletRequest) servletRequest).getRequestURI(), stopwatch.elapsed(TimeUnit.MILLISECONDS));
        }
    }

    @Override
    public void destroy() {

    }
}

package org.tai.spring.wiring.config;

import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:report.properties")
public class PropertySourcesPlaceholderConfigurer {

}

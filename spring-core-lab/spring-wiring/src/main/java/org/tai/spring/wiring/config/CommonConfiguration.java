package org.tai.spring.wiring.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.tai.spring.wiring.application.ReportGenerator;
import org.tai.spring.wiring.application.TransactionService;
import org.tai.spring.wiring.domain.repository.TransactionRepository;
import org.tai.spring.wiring.domain.service.SummaryService;

@Configuration
@PropertySource("classpath:report.properties")
@Profile("common")
@Import({ ProductionConfiguration.class, TestConfiguration.class })
public class CommonConfiguration {

	private @Value("${report.location}") String location;

	@Autowired
	private DataSource dataSource;

	@Bean
	public ReportGenerator reportGenerator() { 
		return new ReportGenerator(transactionService(), location); 
	}

	@Bean
	public TransactionService transactionService() {
		return new TransactionService(transactionRepository(), summaryService());
	}
	
	@Bean
	public TransactionRepository transactionRepository() {
		return new TransactionRepository(dataSource);
	}
	
	@Bean
	public SummaryService summaryService() {
		return new SummaryService();
	}
	
//    @Bean
//    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
//       return new PropertySourcesPlaceholderConfigurer();
//    }
	
}

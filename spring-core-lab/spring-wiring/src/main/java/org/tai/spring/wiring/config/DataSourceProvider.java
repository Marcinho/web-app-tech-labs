package org.tai.spring.wiring.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Configuration;

@Configuration
public interface DataSourceProvider {

	public DataSource dataSource();
	
}

package org.tai.spring.wiring.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
@Profile("test")
public class TestConfiguration implements DataSourceProvider {

	@Override
	@Bean(name = "dataSource")
	public DataSource dataSource() {
		EmbeddedDatabaseBuilder embeddedDatabaseBuilder = new EmbeddedDatabaseBuilder();
		return embeddedDatabaseBuilder.addScript("classpath:schema.sql")
				.addScript("classpath:test-data.sql")
				.setType(EmbeddedDatabaseType.HSQL)
				.build();
	}
	
	

}

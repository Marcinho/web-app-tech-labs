package org.tai.spring.wiring.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
@Profile("production")
public class ProductionConfiguration implements DataSourceProvider {

	@Override
	@Bean(name = "dataSource")
	public DataSource dataSource() {
		EmbeddedDatabaseBuilder embeddedDatabaseBuilder = new EmbeddedDatabaseBuilder();
		return embeddedDatabaseBuilder.addScript("classpath:schema.sql")
				.addScript("classpath:production-data.sql")
				.setType(EmbeddedDatabaseType.HSQL)
				.build();
	}
	
}

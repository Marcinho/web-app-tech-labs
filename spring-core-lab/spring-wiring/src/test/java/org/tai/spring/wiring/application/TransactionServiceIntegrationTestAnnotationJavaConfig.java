package org.tai.spring.wiring.application;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.tai.spring.wiring.config.CommonConfiguration;
import org.tai.spring.wiring.config.TestConfiguration;
import org.tai.spring.wiring.domain.model.Account;
import org.tai.spring.wiring.domain.model.TransactionSummary;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { CommonConfiguration.class,
		TestConfiguration.class }, loader = AnnotationConfigContextLoader.class)
@ActiveProfiles({ "common", "test" })
public class TransactionServiceIntegrationTestAnnotationJavaConfig {

	@Autowired
	private TransactionService underTest;

	@Test
	public void shouldGroupTransactionsByAccounts() {
		// when
		final Collection<TransactionSummary> summaries = underTest.createTrancationSummary();

		Assertions.assertThat(summaries).hasSize(2);
	}

	@Test
	public void shouldSumTransactionsForAccounts() {
		// when
		final Collection<TransactionSummary> summaries = underTest.createTrancationSummary();

		final Collection<TransactionSummary> expected = Arrays.asList(
				new TransactionSummary(Account.of("00000001"), BigDecimal.valueOf(20000, 2)),
				new TransactionSummary(Account.of("99999999"), BigDecimal.valueOf(100, 2)));

		// then
		Assertions.assertThat(summaries).isEqualTo(expected);
	}

}

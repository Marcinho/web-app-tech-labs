package org.tai.spring.scopes.objects;

public class InPrototypeScope {
	
	public void init() {
		System.out.println("Init method in 'InPrototypeScope'");
	}
	
	public void destroy() {
		System.out.println("Destroy method in 'InPrototypeScope'");
	}
	
}

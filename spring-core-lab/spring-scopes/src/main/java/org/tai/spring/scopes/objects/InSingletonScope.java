package org.tai.spring.scopes.objects;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class InSingletonScope implements ApplicationContextAware  {
	
	public void init() {
		System.out.println("Init method in 'InSingletonScope'");
	}
	
	public void destroy() {
		System.out.println("Destroy method in 'InSingletonScope'");
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		
		System.out.println("Beans currently initialized in application context:");
		
		for(String beanName: applicationContext.getBeanDefinitionNames()) {
			System.out.println(beanName);
		}
	}
	
}
